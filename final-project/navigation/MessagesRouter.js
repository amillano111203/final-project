import React from 'react';
import {ScrollView, View, Text, StatusBar} from 'react-native';
import {NativeRouter, Route} from 'react-router-native';
import firebase from 'react-native-firebase';
export default class MessagesRouter extends React.Component{
    render(){
        firebase.messaging().hasPermission().then((enabled)=>{
            if (!enabled)
                firebase.messaging().requestPermission().then(()=>{
                    alert("PERMISSION");
                })
        });
        return (
            <ScrollView style={{width: '100%', height:'100%'}}>
                <StatusBar hidden/>
                <NativeRouter>
                    <View>
                        <Text>Messages</Text>
                    </View>
                </NativeRouter>
            </ScrollView>
        );
    }
}
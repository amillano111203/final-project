import React from 'react';
import TabNavigator from 'react-native-tab-navigator';
import * as firebase from 'firebase';
import 'firebase/firestore';
import {Text, View, ToastAndroid} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Search from '../screens/Search';
import Map from '../screens/Map';
import LoadingScreen from '../screens/LoadingScreen';
import Home from '../screens/Home';
export default class MainTabNavigator extends React.Component{
    state = {selectedTab: 'home', loading: true, concerts: []}
    constructor(props){
        super(props);
        const {user} = this.props;
        console.log("User: ", user.email);
        firebase.firestore().collection('users').doc(user.email).get()
        .then((doc)=>{
            if (doc.exists){
                const {concerts} = doc.data();
                console.log("Conciertos: ", concerts);
                this.setState({concerts, user, loading: false})
            }
        })
        .catch((e)=>{
            alert(e)
        })
    }
    addConcertToUser(concertId, assistanceData){
        const filteredConcerts = this.state.concerts.filter((concert)=>{concert.concertId === concertId})
        if (filteredConcerts.length === 0){
            //For avoiding duplicated values
            const concerts = this.state.concerts.concat([{concertId, assistanceData}]);
            firebase.firestore().collection('users').doc(this.state.user.email).update({
                concerts
            }).then(()=>{
                ToastAndroid.show('Your assistance to the concert was successfully saved', ToastAndroid.SHORT);
                this.setState({concerts});
            }).catch(()=>{
                alert("Something went wrong. Please, try again later");
            })
        }
    }
    modifyAssistanceData(concertId, assistanceData){
        const concerts = this.state.concerts.map((concert)=>{
            if (concert.concertId === concertId){
                return {concertId, assistanceData}
            }else{
                return concert
            }
        });
        firebase.firestore().collection('users').doc(this.state.user.email).update({
            concerts
        }).then(()=>{
            this.setState({concerts});
        }).catch(()=>{
            alert("Something went wrong. Please, try again later");
        })
    }
    removeConcertFromUser(concertId){
        const concerts = this.state.concerts.filter((concert)=>{concert.concertId !== concertId});
        console.log("Concerts después de eliminar", concerts);
        firebase.firestore().collection('users').doc(this.state.user.email).update({
            concerts
        }).then(()=>{
            this.setState({concerts});
        }).catch(()=>{
            alert("Something went wrong. Please, try again later");
        })
    }
    shouldComponentUpdate(nextProps, nextState){
        return this.props !== nextProps || this.state !== nextState
    }
    render(){
        if (this.state.loading === true){
            return(<View style={{width: '100%', height: '100%'}}>
                <LoadingScreen/>
            </View>);
        }
        return(
            <View style={{width: '100%', height: '100%'}}>
                <TabNavigator>
                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'home'}
                        title="Home"
                        renderIcon={() => <Ionicons name='md-home' size={20}/>}
                        renderSelectedIcon={() => <Ionicons name='md-home' size={20} color='#000000'/>}
                        onPress={() => this.setState({ selectedTab: 'home' })}
                    >
                        <View style={{width: '100%', height: '100%'}}>
                            <Home userConcerts={this.state.concerts}/>
                        </View>
                    </TabNavigator.Item>
                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'search'}
                        title="Search"
                        renderIcon={() => <Ionicons name='md-search' size={20}/>}
                        renderSelectedIcon={() => <Ionicons name='md-search' size={20} color='#000000'/>}
                        onPress={() => this.setState({ selectedTab: 'search' })}
                    >
                        <View>                            
                            <Search 
                                userConcerts={this.state.concerts}
                                addConcertToUser={(concertId, assistanceData)=>{this.addConcertToUser(concertId, assistanceData)}}
                                modifyAssistanceData={(concertId, assistanceData)=>{this.modifyAssistanceData(concertId, assistanceData)}}
                                removeConcertFromUser={(concertId)=>{this.removeConcertFromUser(concertId)}}
                                />
                        </View>
                    </TabNavigator.Item>
                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'map'}
                        title="Map"
                        renderIcon={() => <Ionicons name='md-locate' size={20}/>}
                        renderSelectedIcon={() => <Ionicons name='md-locate' size={20} color='#000000'/>}
                        onPress={() => this.setState({ selectedTab: 'map' })}
                    >
                        <Map selectedTab={this.state.selectedTab}/>
                    </TabNavigator.Item>
                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'messages'}
                        title="Messages"
                        renderIcon={() => <Ionicons name='md-chatboxes' size={20}/>}
                        renderSelectedIcon={() => <Ionicons name='md-chatboxes' size={20} color='#000000'/>}
                        onPress={() => this.setState({ selectedTab: 'messages' })}
                    >
                        <View>
                            <Text>Messages</Text>
                        </View>
                    </TabNavigator.Item>
                </TabNavigator>
            </View>
        );
    }
}
import React from 'react';
import {ScrollView, View, Text, StatusBar} from 'react-native';
import {NativeRouter, Route} from 'react-router-native';
export default class HomeRouter extends React.Component{
    //Set route to / on component mount
    render(){
        return (
            <ScrollView style={{width: '100%', height:'100%', alignContent: 'center'}}>
                <StatusBar hidden/>
                <NativeRouter>
                    <View>
                        <Text>Home</Text>
                    </View>
                </NativeRouter>
            </ScrollView>
        );
    }
}
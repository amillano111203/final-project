import React from 'react';
import {ScrollView, View, Text, StatusBar} from 'react-native';
import {NativeRouter, Route} from 'react-router-native';
export default class SearchRouter extends React.Component{
    render(){
        return (
            <ScrollView style={{width: '100%', height:'100%'}}>
                <StatusBar hidden/>
                <NativeRouter>
                    <View>
                        <Text>Search</Text>
                    </View>
                </NativeRouter>
            </ScrollView>
        );
    }
}
import React from 'react';
import {View, StatusBar, ActivityIndicator, StyleSheet, YellowBox} from 'react-native';
import MainTabNavigator from './navigation/MainTabNavigator';
import * as firebase from 'firebase';
import Login from './screens/Login';
import CreateAccount from './screens/CreateAccount';
export default class App extends React.Component{
  constructor(props){
    super(props);
    YellowBox.ignoreWarnings(['Setting a timer']);
    var config = {
      apiKey: "AIzaSyAlEBkWKa8HAEsaZGA-_7CwrkDoekNP_P0",
      authDomain: "showsandco-a7e91.firebaseapp.com",
      databaseURL: "https://showsandco-a7e91.firebaseio.com",
      projectId: "showsandco-a7e91",
      storageBucket: "showsandco-a7e91.appspot.com",
      messagingSenderId: "265054397437"
    };
    firebase.initializeApp(config);
    //firebase.messaging().usePublicVapidKey("BPZ7VgWkzIpMGwHOwU7QWlng4oWVST9rJ8ZgcaAlGG5etYrKpRICUwelBvZ4hm5yRjZyF_B810TQ4PxNV_lfYGU");
    firebase.auth().onAuthStateChanged((user) => {
      //Comment this to disable auto-login
      if (user) {
        this.setState({loginOK: true, loading: false});
      }else{
        this.setState({loading: false});
      }
      /**
      this.setState({loading:false});*/
    })
    this.state= {loginOK: false, user: '', password: '', rememberMe: false, loading:true, mode: "login"};
  }
  shouldComponentUpdate(nextProps, nextState){
    return nextProps !== this.props || nextState !== this.state
  }
  render(){
    if (this.state.loading){
      const styles = StyleSheet.create({
        container: {
          flex: 1,
          justifyContent: 'center'
        },
        horizontal: {
          flexDirection: 'row',
          justifyContent: 'space-around',
          padding: 10
        }
      })
      return(
        <View style = {[styles.container, styles.horizontal]}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    }
    if (this.state.loginOK){
      return (
        <View>
          <StatusBar hidden/>
          <MainTabNavigator user={firebase.auth().currentUser}/>
        </View>
      );
    } else {
      if (this.state.mode === "login")
        return(
          <Login 
            changeMode={(mode)=>{this.setState({mode})}}
            changeUser={(user)=>{this.setState({user})}}
            changePassword={(password)=>{this.setState({password})}}
            changeRememberMe={()=>{this.setState({rememberMe: !this.state.rememberMe})}}
            changeLoginOK={(loginOK)=>{this.setState({loginOK})}}
            user={this.state.user}
            password={this.state.password}
            rememberMe={this.state.rememberMe}
            />
        );
      else if (this.state.mode === "createAccount"){
        return(
          <CreateAccount 
            changeUser={(user)=>{this.setState({user})}}
            changePassword={(password)=>{this.setState({password})}}
            changeMode={(mode)=>{this.setState({mode})}}
            user={this.state.user}
            password={this.state.password}
          />);
      }
    }
  }
}
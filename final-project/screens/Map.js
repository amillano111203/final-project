import React from 'react';
import {View, Text, BackHandler} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import Picker from 'react-native-picker-js';
import DetailedConcert from './DetailedConcert';
import LoadingScreen from './LoadingScreen';
var Geohash = require('latlon-geohash');
const TICKET_MASTER_KEY = '6S5ACXmzudHKeEE0t8aRj0Zw9hzoyWzs';
export default class Map extends React.Component{
    state={
        venues: [],
        events: [''],
        selectedEvent: 0,
        loading: true,
        center: {
            latitude: 0,
            longitude: 0
        },
        showDialog: false,
        eventToShow: null,
        detailedEvents: []
    };
    constructor(props){
        super(props);
        BackHandler.addEventListener('hardwareBackPress', ()=>{
            if (this.state.eventToShow !== null && this.props.selectedTab === 'map'){
                this.setState({eventToShow: null});
                return true;
            }
            return false;
        });
        navigator.geolocation.getCurrentPosition((position)=>{
            const hash = Geohash.encode(position.coords.latitude, position.coords.longitude, 9);
            fetch(`https://app.ticketmaster.com/discovery/v2/venues?apikey=${TICKET_MASTER_KEY}&geoPoint=${hash}`)
            .then((response)=>{
                response.json().then((json)=>{
                    if (json._embedded)
                        this.setState({venues: json._embedded.venues, loading: false, center: position.coords})
                    else
                        this.setState({venues: [], loading: false});
                }).catch((e)=>{
                    console.error(e);
                })
            }).catch((e)=>{
                console.error(e);
            })
        },
        ()=>{
            alert("Something went wrong. Please, try again later");
        });
    }
    shouldComponentUpdate(nextProps, nextState){
        return this.state.venues !== nextState.venues || 
                this.state.events !== nextState.events || 
                this.state.eventToShow !== nextState.eventToShow ||
                this.props !== nextProps
    }
    renderMapIcons(){
        const {venues} = this.state;
        return venues.map((venue, key)=>{
            const coordinate = {
                latitude: parseFloat(venue.location.latitude),
                longitude: parseFloat(venue.location.longitude)
            }
            return <Marker
                key={key}
                coordinate={coordinate}
                title={venue.name}
                description={`Click on the marker to see next concerts in ${venue.name}`}
                onCalloutPress={
                    ()=>{
                        this.setState({events: ['Loading...'], selectedEvent: 0})
                        this.state.picker.show();
                        fetch(`https://app.ticketmaster.com/discovery/v2/events?apikey=${TICKET_MASTER_KEY}&venueId=${venue.id}`)
                        .then(response => {
                            response.json().then((json)=>{
                                if (json._embedded){
                                    const events = json._embedded.events.map((event)=>{
                                        return event.name
                                    })
                                    const detailedEvents = json._embedded.events;
                                    this.setState({events, detailedEvents, selectedEvent: 0})
                                }else{
                                    this.setState({events: [''], selectedEvent: 0, detailedEvents: []});
                                    alert(`There are no upcoming events in ${venue.name}`);
                                }
                            })
                        })
                    }
                }
            >
            </Marker>
        })
    }
    render(){
        if (this.state.loading === true){
            return (<View>
                <LoadingScreen/>
            </View>)
        }
        if (this.state.eventToShow !== null){
            return (<View>
                <DetailedConcert concert={this.state.eventToShow}/>
            </View>)
        }
        return(
            <View>
                <MapView
                    style={{width: "100%", height: "100%"}}
                    initialRegion={{
                        latitude: this.state.center.latitude,
                        longitude: this.state.center.longitude,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    }}
                >
                    {this.renderMapIcons()}
                </MapView>
                <Picker
                    ref={picker => this.setState({picker})}
                    pickerBtnText="Details"
                    pickerBtnStyle={{width: '100%'}}
                    showMask={true}
                    pickerData={this.state.events}//picker`s value List
                    selectedValue={this.state.events[this.state.selectedEvent]}//Almacenarlo como un numero en el estado y acceder asi al array de nombres y al de conciertos
                    onPickerDone={()=>{
                        if (this.state.events !== ['Loading...'])
                            this.setState({eventToShow: this.state.detailedEvents[this.state.selectedEvent]});
                    }}
                    onValueChange={(newValue)=>{
                        this.setState({selectedEvent: this.state.events.indexOf(newValue[0])});
                    }}
                />
            </View>)
    }

}
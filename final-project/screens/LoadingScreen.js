import React from 'react';
import {Animated, View, ActivityIndicator, Text} from 'react-native';
export default class LoadingScreen extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <View style={{ paddingTop: 25, marginTop: '50%', flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Animated.View>
                    <ActivityIndicator size="large" color="#000" />
                    <Text>Loading...</Text>
                </Animated.View>
            </View>
        );
    }
}
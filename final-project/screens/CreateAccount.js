import React from 'react';
import {View, StatusBar, Button, TextInput, TouchableOpacity, Text} from 'react-native';
import * as firebase from 'firebase';
import 'firebase/firestore';
export default class Login extends React.Component{
    shouldComponentUpdate(nextProps, nextState){
        return this.props!==nextProps || this.state!==nextState
    }
    render (){
        return(
        <View style={{alignContent: 'center'}}>
          <StatusBar hidden/>
          <TextInput style={{margin: '5%', paddingLeft: '2%', borderColor: '#000000', borderWidth: 1}} placeholder={'Email'} onChangeText={(user)=>{
            this.props.changeUser(user);
          }}/>
          <TextInput style={{margin: '5%', paddingLeft: '2%', borderColor: '#000000', borderWidth: 1}} secureTextEntry={true} placeholder={'Password'} onChangeText={(password)=>{
            this.props.changePassword(password);
          }}/>
          <Button 
            title={"Create Account"} 
            onPress={()=>{  
                firebase.auth().createUserWithEmailAndPassword(this.props.user, this.props.password).then((result)=>{
                    result.user.sendEmailVerification();
                    firebase.auth().signOut().then(()=>{
                        alert("Your account has been successfully created. Please, verify your email to start using Shows&Co.");
                        this.props.changeMode("login");
                        firebase.firestore().collection('users').doc(result.user.email).set({concerts: []}).then(()=>{
                            console.log("OK");
                        }).catch((e)=>{
                            console.log(e);
                        })
                    })
                }).catch((e)=>{
                    alert(e);
                })
            }
            }/>
            <View style={{alignItems:'center', paddingTop: "5%"}}>
                <TouchableOpacity onPress={()=>{this.props.changeMode("login")}}>
                    <Text>I already have an account</Text>
                </TouchableOpacity>
            </View>
        </View>);
    }
}
import React from 'react';
import {View} from 'react-native';
import {Card, CardTitle, CardImage} from 'react-native-material-cards';
import MapView, { Marker } from 'react-native-maps';
import AssistanceForm from '../components/AssistanceForm';
const OUTTER_CARD_COLOR = '#4682B4';
const INNER_CARD_COLOR = '#3f5e77';
export default class DetailedConcert extends React.Component{
    constructor(props){
        super(props);
    }
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return this.state !== nextState || this.props !== nextProps || this.context !== nextContext
    }

    render(){
        const {concert, assistanceData} = this.props;
        const {accomodation, transport} = assistanceData;
        console.log(`Concert: ${concert.name}\nAccomodation: ${accomodation}\nTransport: ${transport}`);
        const venue = concert._embedded.venues[0];
        const coordinate = {
            latitude: parseFloat(venue.location.latitude),
            longitude: parseFloat(venue.location.longitude)
        }
        const seatMap = concert.seatmap ? <Card isDark style={{backgroundColor: INNER_CARD_COLOR}}>
                            <CardTitle title={venue.name}/>
                            <CardImage source={{uri:concert.seatmap.staticUrl}}/>
                        </Card>:<View></View>
        return(
            <Card isDark style={{backgroundColor: OUTTER_CARD_COLOR}}>
                <CardTitle 
                    title={concert.name}
                    />
                <CardImage 
                    source={{uri: concert.images[0].url}} 
                    />
                <AssistanceForm 
                    assistanceData={{accomodation, transport}}
                    concertId={concert.id}
                    addConcertToUser={(concertId, assistanceData)=>{this.props.addConcertToUser(concertId, assistanceData)}}
                    modifyAssistanceData={(concertId, assistanceData)=>{this.props.modifyAssistanceData(concertId, assistanceData)}}
                    removeConcertFromUser={(concertId)=>{this.props.removeConcertFromUser(concertId)}}            
                    />
                {concert.seatmap ? <Card isDark style={{backgroundColor: INNER_CARD_COLOR}}>
                            <CardTitle title={venue.name}/>
                            <CardImage source={{uri:concert.seatmap.staticUrl}}/>
                        </Card>:<View/>}                
            </Card>
        );
    }
}
import React from 'react';
import {View, ScrollView, Text, TouchableOpacity} from 'react-native';
import LoadingScreen from './LoadingScreen';
import ConcertPreview from './../components/ConcertPreview';
const TICKET_MASTER_KEY = '6S5ACXmzudHKeEE0t8aRj0Zw9hzoyWzs';
export default class Home extends React.Component{
    state = {
        loading: true,
        concerts: [],
        selectedConcert: -1
    }
    constructor(props){
        super(props);
    }
    componentDidMount(){
        const {userConcerts} = this.props;
        this.getConcertsByIds(userConcerts);
    }
    shouldComponentUpdate(nextProps, nextState){
        return this.props !== nextProps || this.state !== nextState;
    }
    selectConcert(index){
        this.setState({selectedConcert: this.state.concerts[index]});
    }
    renderConcerts(){
        const {concerts} = this.state;
        return concerts.map((concert, index)=>{
            console.log(`(${index}) Rendering concert ${concert.name}`)
            return <ConcertPreview 
                key={index} 
                index={index}
                concert={concert}
                assistanceData={this.props.userConcerts.filter((userConcert)=>userConcert.concertId === concert.id)[0].assistanceData}
                selectConcert={(index)=>{this.selectConcert(index)}}
                />
        })
    }
    componentDidUpdate(prevProps, prevState, snapshot){
        if (prevProps.userConcerts !== this.props.userConcerts){
            //The user made some change on the db (adding, removing or updating assistance data)
            this.getConcertsByIds(this.props.userConcerts);
        }
    }
    getConcertsByIds(userConcerts){
        if (userConcerts.length > 0){
            let idsParam = '';
            userConcerts.forEach((concert)=>{
                idsParam = idsParam + `&id=${concert.concertId}`; 
            });
            fetch(`https://app.ticketmaster.com/discovery/v2/events.json?apikey=${TICKET_MASTER_KEY}${idsParam}`)
            .then((response)=>{
                response.json().then((json)=>{
                    if (json._embedded){
                        this.setState({concerts: json._embedded.events, loading: false});
                    }
                }).catch((e)=>{
                    console.error(e);
                })
            }).catch((e)=>{
                console.error(e);
            });   
        }
    }
    render(){
        const {userConcerts} = this.props;
        if (this.state.loading === true){
            return <LoadingScreen />
        }
        if (userConcerts.length === 0){
            return <View>
                <Text>Go to the Search tab and you'll see your future events here!</Text>
            </View>
        }
        return(<ScrollView>
            {this.renderConcerts()}
        </ScrollView>);
    }
}
import React from 'react';
import {View, StatusBar, TouchableOpacity, Text} from 'react-native';
//import {CheckBox} from 'react-native-elements';
import { Button } from 'react-native-material-ui';import { TextField } from 'react-native-materialui-textfield';
import * as firebase from 'firebase';
import 'firebase/firestore';
export default class Login extends React.Component{
    shouldComponentUpdate(nextProps, nextState){
        return this.props!==nextProps || this.state!==nextState
    }
    render (){
        return(
        <View style={{alignContent: 'center'}}>
          <StatusBar hidden/>
          <View style={{marginLeft: '5%', marginRight: '5%', paddingLeft: '2%'}}>
            <TextField label={'Email'} value={this.props.user} onChangeText={(user)=>{
              this.props.changeUser(user);
            }}/>
          </View>
          <View style={{marginLeft: '5%', marginRight: '5%', paddingLeft: '2%'}}>
            <TextField secureTextEntry={true} value={this.props.password} label={'Password'} onChangeText={(password)=>{
              this.props.changePassword(password);
            }}/>
          </View>
          <Button 
            raised
            primary 
            text="Login"
            onPress={()=>{
              if (this.props.rememberMe){
                firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
                .then(()=>{
                  firebase.auth().signInWithEmailAndPassword(this.props.user, this.props.password).then((result)=>{
                    if (result.user.emailVerified){
                      firebase.firestore().collection('users').doc(result.user.email).get().then((doc)=>{
                        if (doc.exists){
                          this.props.changeLoginOK(true);
                        }
                      });
                    }else{
                      alert("Please, verify your email to continue");
                    }
                  }).catch((e)=>{
                    alert(e);
                  })  
                });
              }else{
                firebase.auth().signInWithEmailAndPassword(this.props.user, this.props.password).then((result)=>{
                  if (result.user.emailVerified){
                    firebase.firestore().collection('users').doc(result.user.email).get().then((doc)=>{
                        if (doc.exists){
                          this.props.changeLoginOK(true);
                        }
                      });
                  }else{
                    firebase.auth().signOut().then(()=>{
                      alert("Please, verify your email to continue");
                    })
                  }
                }).catch((e)=>{
                  alert(e);
                })
              }
            }}/>
            <View style={{alignItems:'center', paddingTop: "5%"}}>
                <TouchableOpacity onPress={()=>{this.props.changeMode("createAccount")}}>
                    <Text>Create an account</Text>
                </TouchableOpacity>
            </View>
        </View>);
    }
}
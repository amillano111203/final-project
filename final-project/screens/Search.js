import React from 'react';
import {ScrollView, View, TouchableOpacity, BackHandler, Keyboard} from 'react-native';
import ConcertPreview from '../components/ConcertPreview';
import {SearchBar} from 'react-native-elements';
import {Button, Icon} from 'react-native-material-ui';
import DetailedConcert from './DetailedConcert';
import LoadingScreen from './LoadingScreen';
const TICKET_MASTER_KEY = '6S5ACXmzudHKeEE0t8aRj0Zw9hzoyWzs';
export default class Search extends React.Component{
    state={key: '', concerts: [], loading: false, selectedConcert: -1, showLoadingScreen: true}
    constructor(props){
        super(props);
        fetch(`https://app.ticketmaster.com/discovery/v2/events.json?apikey=${TICKET_MASTER_KEY}`)
        .then((response)=>{
            response.json().then((json)=>{
                if (json._embedded){
                    this.setState({concerts: json._embedded.events, loading: false, selectedConcert: -1, showLoadingScreen: false})
                }else{
                    this.setState({concerts: [], loading: false, showLoadingScreen: false});
                }
            }).catch((e)=>{
                console.error(e);
            })
        }).catch((e)=>{
            console.error(e);
        })
    }
    shouldComponentUpdate(nextProps, nextState){
        return this.props !== nextProps || this.state !== nextState;
    }
    searchByKey(key){
        fetch(`https://app.ticketmaster.com/discovery/v2/events.json?apikey=${TICKET_MASTER_KEY}&keyword=${key}`)
        .then((response)=>{
            response.json().then((json)=>{
                if (json._embedded){
                    console.log(`${json._embedded.events.length} events found for "${key}"`)
                    this.setState({concerts: json._embedded.events, loading: false, selectedConcert: -1})
                }else{
                    this.setState({concerts: [], loading: false});
                }
            }).catch((e)=>{
                console.error(e);
            })
        }).catch((e)=>{
            console.error(e);
        })
    }
    renderResults(){
        const {concerts} = this.state;
        return concerts.map((concert, index)=>{
            console.log(`(${index}) Rendering concert ${concert.name}`)
            return <View key={index}>
                <ConcertPreview index={index} selectConcert={(index)=>{this.setState({selectConcert: index})}} concert={concert}/>
            </View>
        })
    }
    render(){
        const {selectedConcert} = this.state;
        if (this.state.showLoadingScreen === true){
            return(<LoadingScreen/>);
        }
        if (selectedConcert===-1){
            BackHandler.removeEventListener('hardwareBackPress');
            BackHandler.addEventListener('hardwareBackPress', ()=>{
                this.setState({key: '', concert:[], selectedConcert: -1});
                BackHandler.removeEventListener('hardwareBackPress');
            })
            return (
                <View>
                    <View style={{flexDirection: "row", alignItems: 'center', justifyContent: 'center'}}>
                        <View style={{width: '80%', height: '100%'}}>
                            <SearchBar 
                                round
                                lightTheme
                                showLoading={this.state.loading}
                                placeholder={"Type here your concert..."}
                                onChangeText={(key)=>{
                                        this.setState({key});
                                    }
                                }
                                onClear={()=>{
                                    this.setState({key: '', concerts: []})
                                }}
                                value={this.state.key}
                                ></SearchBar>
                        </View>
                        <View style={{flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center'}}>
                            <Button 
                                style={{container:{backgroundColor: '#4682B4'}}} 
                                text=""
                                icon={<Icon name='search'></Icon>}
                                raised
                                onPress={()=>{
                                    Keyboard.dismiss();
                                    this.setState({loading: true})
                                    this.searchByKey(this.state.key)
                                }}/>
                        </View>
                    </View>
                    <ScrollView>
                        {this.renderResults()}
                    </ScrollView>
                </View>
            );
        }else{
            const concert = this.state.concerts[selectedConcert];
            let assistanceData = this.props.userConcerts.map((userConcert)=>{
                if (userConcert.concertId === concert.id){
                    return userConcert.assistanceData
                }
            })[0];
            assistanceData = assistanceData || {accomodation: 0, transport: 0};
            console.log("Assistance data: ", assistanceData)
            BackHandler.addEventListener('hardwareBackPress', ()=>{
                if (this.state.selectedConcert!==-1){
                    this.setState({selectedConcert: -1});
                    return true;
                }
                return false;
            });
            return(
                <ScrollView>
                    <DetailedConcert 
                        concert={concert} 
                        assistanceData={assistanceData}
                        addConcertToUser={(concertId, assistanceData)=>{this.props.addConcertToUser(concertId, assistanceData)}}
                        modifyAssistanceData={(concertId, assistanceData)=>{this.props.modifyAssistanceData(concertId, assistanceData)}}
                        removeConcertFromUser={(concertId)=>{this.props.removeConcertFromUser(concertId)}}        
                        />
                </ScrollView>);
        }
    }
}
import React from 'react';
import {Card} from 'react-native-material-cards';
import SwitchSelector from "react-native-switch-selector";
import MainTabNavigator from './../navigation/MainTabNavigator';
export default class AssistanceForm extends React.Component{
    state={imGoing: true}
    constructor(props){
        super(props);
    }
    shouldComponentUpdate(nextProps, nextState){
        return this.props !== nextProps || this.state !== nextState
    }
    render(){
        const {assistanceData, concertId} = this.props;
        const {accomodation, transport} = assistanceData;
        console.log(`Concert: ${concertId}\nAccomodation: ${accomodation}\nTransport: ${transport}`);
        const {imGoing} = this.state;
        return(<Card isDark style={{backgroundColor: '#3f5e77'}}>
                    <SwitchSelector
                        initial={this.state.imGoing ? 0 : 1}
                        options={[{label: "I'm going!", value: true}, {label: "I'm not going", value: false}]}
                        onPress={(imGoing)=>{
                            if (imGoing){
                                this.props.addConcertToUser(concertId, {accomodation, transport});
                            } else {
                                this.props.removeConcertFromUser(concertId);
                            }
                            this.setState({imGoing})}
                        }
                        style={{padding: '5%'}}
                        hasPadding
                    />
                    <SwitchSelector 
                        initial={0}
                        options={[
                            {label: 'Not offering nor looking for accomodation', value: 0},
                            {label: 'Offering accomodation', value: 1},
                            {label: 'Looking for accomodation', value: 2}
                        ]}
                        onPress={(accomodation)=>{
                            this.props.modifyAssistanceData(concertId, {accomodation, transport});
                            this.setState({accomodation})}
                        }
                        disabled={!imGoing}
                        buttonColor={imGoing ? '#BCD635' : 'grey'}
                        style={{padding: '5%', paddingTop: 0}}
                        hasPadding
                        height={60}
                    />
                    <SwitchSelector 
                        initial={0}
                        options={[
                            {label: 'Not offering nor looking for transport', value: 0},
                            {label: 'Offering transport', value: 1},
                            {label: 'Looking for transport', value: 2}
                        ]}
                        hasPadding
                        disabled={!imGoing}
                        buttonColor={imGoing ? '#BCD635' : 'grey'}
                        onPress={(transport)=>{
                            this.props.modifyAssistanceData(concertId, {accomodation, transport});
                            this.setState({transport})}
                        }
                        style={{padding: '5%', paddingTop: 0}}
                        height={60}
                    />
            </Card>);
    }
}
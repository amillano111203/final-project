import React from 'react';
import {View, Text} from 'react-native';
import {Card, CardTitle, CardImage, CardButton, CardAction} from 'react-native-material-cards';
export default class ConcertPreview extends React.Component{
    constructor(props){
        super(props);
    }
    renderExtraData(){
        const {assistanceData, concert} = this.props;
        if (assistanceData){
            const accomodationType = ['', 'You are offering accomodation', 'You are looking for accomodation'];
            const transportType = ['', 'You are offering transport', 'You are looking for transport'];
            return (<View>
                {assistanceData.accomodation > 0 ? <CardTitle
                    isDark
                    subtitle={accomodationType[assistanceData.accomodation]}
                />:<View/>}
                {assistanceData.transport > 0 ? <CardTitle
                    isDark
                    subtitle={transportType[assistanceData.transport]}
                />:<View/>}
            </View>) 
        }else{
            return <CardImage 
                source={{uri: concert.images[0].url}} 
            />
        }
    }
    render(){
        const {concert, index, assistanceData} = this.props;
        const location = concert._embedded.venues[0];
        const accomodationType = ['', 'You are offering accomodation', 'You are looking for accomodation'];
        const transportType = ['', 'You are offering transport', 'You are looking for transport'];    
        return(
            <Card isDark style={{backgroundColor: '#4682B4'}}>
                <CardTitle 
                    title={concert.name} 
                    subtitle={`${location.name} - ${location.city.name} (${location.country.name})`}
                />
                {assistanceData ? <Text style={{fontSize:14,
                                                color: 'rgba(255 ,255 ,255 , 0.87)'}}>
                                                    {accomodationType[assistanceData ? assistanceData.accomodation : 0]}
                                                </Text>:<View style={{width: 0, height: 0}}/>}
                <CardAction>
                    <CardButton title="View more" onPress={()=>{this.props.selectConcert(index)}}/>
                </CardAction>
            </Card>
        );
    }
}
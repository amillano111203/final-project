import React from 'react';
import { Dropdown } from 'rn-material-dropdown';
const NO_ACCOMODATION = '-';
const ACCOMODATION_NEEDED = "I'm looking for accomodation";
const ACCOMODATION_OFFERED = "I'm offering accomodation";
export default class AccomodationDropdown extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        const {accomodation} = this.props;
        let value = '';
        switch(accomodation){
            case 0:
                value = NO_ACCOMODATION;
                break;
            case 1:
                value = ACCOMODATION_NEEDED;
                break;
            case 2:
                value = ACCOMODATION_OFFERED;
                break;

        }
        //const {onChange} = this.props;
        const accomodationOptions = [
            {
                value: NO_ACCOMODATION
            },{
                value: ACCOMODATION_NEEDED
            },{
                value: ACCOMODATION_OFFERED
            }
        ];
        return(
            <Dropdown
                label='Accomodation'
                data={accomodationOptions}
                value={value}
                onChangeText={(value, index, data)=>{
                    console.log(`.`)
                }}
            />
        );
    }
}
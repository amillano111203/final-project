# /home/inixio/Proyectos/final-project/final-project

Date : 2019-04-09 08:59:48

Total : 27 files,  10383 codes, 16 comments, 8 blanks, all 10407 lines

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 5 | 9,537 | 6 | 3 | 9,546 |
| JavaScript | 15 | 589 | 10 | 4 | 603 |
| XML | 6 | 250 | 0 | 0 | 250 |
| Ignore | 1 | 7 | 0 | 1 | 8 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 27 | 10,383 | 16 | 8 | 10,407 |
| .idea | 6 | 250 | 0 | 0 | 250 |
| components | 3 | 87 | 1 | 1 | 89 |
| navigation | 5 | 131 | 1 | 0 | 132 |
| screens | 5 | 283 | 1 | 2 | 286 |

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [.gitignore](file:///home/inixio/Proyectos/final-project/final-project/.gitignore) | Ignore | 7 | 0 | 1 | 8 |
| [.idea/encodings.xml](file:///home/inixio/Proyectos/final-project/final-project/.idea/encodings.xml) | XML | 4 | 0 | 0 | 4 |
| [.idea/final-project.iml](file:///home/inixio/Proyectos/final-project/final-project/.idea/final-project.iml) | XML | 9 | 0 | 0 | 9 |
| [.idea/misc.xml](file:///home/inixio/Proyectos/final-project/final-project/.idea/misc.xml) | XML | 6 | 0 | 0 | 6 |
| [.idea/modules.xml](file:///home/inixio/Proyectos/final-project/final-project/.idea/modules.xml) | XML | 8 | 0 | 0 | 8 |
| [.idea/vcs.xml](file:///home/inixio/Proyectos/final-project/final-project/.idea/vcs.xml) | XML | 6 | 0 | 0 | 6 |
| [.idea/workspace.xml](file:///home/inixio/Proyectos/final-project/final-project/.idea/workspace.xml) | XML | 217 | 0 | 0 | 217 |
| [.watchmanconfig](file:///home/inixio/Proyectos/final-project/final-project/.watchmanconfig) | JSON | 1 | 0 | 1 | 2 |
| [App.js](file:///home/inixio/Proyectos/final-project/final-project/App.js) | JavaScript | 82 | 7 | 0 | 89 |
| [app.json](file:///home/inixio/Proyectos/final-project/final-project/app.json) | JSON | 23 | 6 | 0 | 29 |
| [babel.config.js](file:///home/inixio/Proyectos/final-project/final-project/babel.config.js) | JavaScript | 6 | 0 | 1 | 7 |
| [components/AccomodationDropdown.js](file:///home/inixio/Proyectos/final-project/final-project/components/AccomodationDropdown.js) | JavaScript | 44 | 1 | 1 | 46 |
| [components/AssistanceForm.js](file:///home/inixio/Proyectos/final-project/final-project/components/AssistanceForm.js) | JavaScript | 19 | 0 | 0 | 19 |
| [components/ConcertPreview.js](file:///home/inixio/Proyectos/final-project/final-project/components/ConcertPreview.js) | JavaScript | 24 | 0 | 0 | 24 |
| [google-services.json](file:///home/inixio/Proyectos/final-project/final-project/google-services.json) | JSON | 42 | 0 | 0 | 42 |
| [navigation/HomeRouter.js](file:///home/inixio/Proyectos/final-project/final-project/navigation/HomeRouter.js) | JavaScript | 17 | 1 | 0 | 18 |
| [navigation/MainTabNavigator.js](file:///home/inixio/Proyectos/final-project/final-project/navigation/MainTabNavigator.js) | JavaScript | 63 | 0 | 0 | 63 |
| [navigation/MapRouter.js](file:///home/inixio/Proyectos/final-project/final-project/navigation/MapRouter.js) | JavaScript | 17 | 0 | 0 | 17 |
| [navigation/MessagesRouter.js](file:///home/inixio/Proyectos/final-project/final-project/navigation/MessagesRouter.js) | JavaScript | 17 | 0 | 0 | 17 |
| [navigation/SearchRouter.js](file:///home/inixio/Proyectos/final-project/final-project/navigation/SearchRouter.js) | JavaScript | 17 | 0 | 0 | 17 |
| [package-lock.json](file:///home/inixio/Proyectos/final-project/final-project/package-lock.json) | JSON | 9,437 | 0 | 1 | 9,438 |
| [package.json](file:///home/inixio/Proyectos/final-project/final-project/package.json) | JSON | 34 | 0 | 1 | 35 |
| [screens/ChatList.js](file:///home/inixio/Proyectos/final-project/final-project/screens/ChatList.js) | JavaScript | 18 | 0 | 0 | 18 |
| [screens/CreateAccount.js](file:///home/inixio/Proyectos/final-project/final-project/screens/CreateAccount.js) | JavaScript | 48 | 0 | 0 | 48 |
| [screens/DetailedConcert.js](file:///home/inixio/Proyectos/final-project/final-project/screens/DetailedConcert.js) | JavaScript | 35 | 0 | 2 | 37 |
| [screens/Login.js](file:///home/inixio/Proyectos/final-project/final-project/screens/Login.js) | JavaScript | 77 | 1 | 0 | 78 |
| [screens/Search.js](file:///home/inixio/Proyectos/final-project/final-project/screens/Search.js) | JavaScript | 105 | 0 | 0 | 105 |